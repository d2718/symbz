#!/bin/sh

TGT='dan@d2718.net:~/wr/symbz/'

scp symbz.html "${TGT}index.html"
scp symbz.min.js $TGT
scp symbz.css $TGT
scp data.json $TGT
scp data/*.txt "$TGT/data/"
