# updata.nu
#
# uploads data files that have changed

let checksum_file = "checksums.nuon"
let data_dir = "data"
let mkdata = "mkdata.nu"
let target = "dan@d2718.net:~/wr/symbz/data/"
let master_data = "data.json"
let master_data_target = "dan@d2718.net:~/wr/symbz/"

nu $mkdata

let old_cksums = try { open $checksum_file } catch { [] }
let cur_cksums = ls $data_dir |
  each {|f| md5sum $f.name} |
  split column --regex '\s+' |
  rename hash file

def bad_hash [datrow: record] {
  $old_cksums |
    filter {|row| $row == $datrow } |
    is-empty
}

def upload_path [pathname: string] {
  print $"Uploading ($pathname)..."
  scp $pathname $target
}

let uploaded = $cur_cksums |
  filter {|datrow| bad_hash $datrow } |
  each { |fpath| upload_path $fpath.file }

if ($uploaded | is-not-empty) {
  print $"Uploading master data file ($master_data)..."
  scp $master_data $master_data_target
}

$cur_cksums | save -f $checksum_file
