#!/home/dan/.cargo/nu

# mkdata.nu
#
# Turns the master `yaml` data file into separate category-specific,
# line-oriented data files.

let target_dir = "data"
let master_file = "symbz.yaml"

def write_category [cat: record] {
  let target_file = $target_dir |
    path join ($cat.name | str snake-case) |
    append ".txt" |
    str join
    
  $cat.symbols |
    each {|sym| $"($sym.value) ($sym.name)"} |
    save -f $target_file

  { name: $cat.name, url: $target_file }
}

def main [] {
  open $master_file |
    each {|cat| write_category $cat} |
    save -f data.json
}
