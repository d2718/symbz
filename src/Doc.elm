module Doc exposing (Cat, Sym, decoderCat, decoderSym, viewCat, viewSym)

import Hex
import Html exposing (Html)
import Html.Attributes as Attributes
import Html.Events as Events
import Yaml.Decode as Decode exposing (Decoder)


decoderHex : Decoder Int
decoderHex =
    Decode.map
        (String.slice 1 -1
            >> Hex.fromString
            >> Result.withDefault 0
        )
        Decode.string


type alias Sym =
    { value : Int
    , name : String
    }


symString : Sym -> String
symString s =
    Char.fromCode s.value |> String.fromChar


thumbSym : (Sym -> a) -> Sym -> Html a
thumbSym update s =
    Html.div
        [ Attributes.class "thumb"
        , Attributes.title s.name
        , Events.onClick (update s)
        ]
        [ Html.span [] [ symString s |> Html.text ] ]


viewSym : Sym -> Html a
viewSym s =
    Html.div
        [ Attributes.id "details" ]
        [ Html.span [ Attributes.class "big" ] [ symString s |> Html.text ]
        , Html.span [ Attributes.class "name" ] [ Html.text s.name ]
        , Html.span [ Attributes.class "sub" ]
            [ "0x" ++ Hex.toString s.value |> Html.text ]
        , Html.span [ Attributes.class "sub" ]
            [ String.fromInt s.value |> Html.text ]
        ]


decoderSym : Decoder Sym
decoderSym =
    Decode.map2 Sym
        (Decode.field "value" decoderHex)
        (Decode.field "name" Decode.string)


type alias Cat =
    { name : String
    , symbols : List Sym
    }


viewCat : (Sym -> a) -> Cat -> Html a
viewCat update cat =
    Html.div [ Attributes.class "category" ]
        [ Html.h2 [] [ Html.text cat.name ]
        , Html.div [ Attributes.class "symbols" ]
            (List.map (thumbSym update) cat.symbols)
        ]


decoderCat : Decoder Cat
decoderCat =
    Decode.map2 Cat
        (Decode.field "name" Decode.string)
        (Decode.field "symbols" (Decode.list decoderSym))
