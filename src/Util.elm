module Util exposing(..)

import Http

indexedTuples : List a -> List (Int, a)
indexedTuples list = List.indexedMap (\n x -> (n, x)) list

rrrMap : (a -> Result e b) -> List a -> Result e (List b)
rrrMap f list =
    let
        lifted : List b -> List a -> Result e (List b)
        lifted acc dsp =
            case dsp of
                [] ->
                    List.reverse acc |> Ok

                x :: rest ->
                    case f x of
                        Ok y ->
                            lifted (y :: acc) rest

                        Err err ->
                            Err err
    in
    lifted [] list

replace : (a -> a -> Bool) -> a -> List a -> List a
replace cmpr elt list =
    List.map (\x -> if cmpr x elt then elt else x) list

httpErrMsg : Http.Error -> String
httpErrMsg e =
    case e of
        Http.BadUrl s -> "bad URL: " ++ s
        Http.Timeout -> "HTTP request timeout"
        Http.NetworkError -> "network error"
        Http.BadStatus n -> "HTTP request failure status: " ++ (String.fromInt n)
        Http.BadBody s -> "bad body: " ++ s
