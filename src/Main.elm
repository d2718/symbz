module Main exposing (main)

import Browser
import Html exposing (Html)
import Html.Attributes as Attributes
import Http
import Util
import Json.Decode as Decode
import Sym exposing (Sym)
import Cat exposing (Cat(..))


dataUrl : String
dataUrl =
    "data.json"


main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = \_ -> Sub.none
        , view = view
        }



-- The State

type Model
    = Loading
    | Ready { cats : List Cat, zoom : Maybe Sym }
    | Error String


type Msg
    = Loaded (Result Http.Error (List Cat.Stub))
    | LoadCat Cat.Stub
    | CatLoaded Model
    | Zoom Sym
    | HideCat Cat.Shown
    | ShowCat Cat.Shown


init : () -> ( Model, Cmd Msg )
init _ =
    ( Loading
    , Http.get
        { url = dataUrl
        , expect = Http.expectJson Loaded (Decode.list Cat.decoder)
        }
    )


-- Updating

hydrateCat : List Cat -> Maybe Sym -> Cat.Stub -> Result Http.Error String -> Msg
hydrateCat cats zoom stub response =
    case response of
        Err e -> (Util.httpErrMsg e |> Error |> CatLoaded)
        Ok data ->
            case Cat.fromData stub data of
                Err e -> Error e |> CatLoaded
                Ok shown ->
                    let newCats = Util.replace (\x y -> Cat.url x == stub.url)
                                                (Cat.Loaded shown) cats
                    in Ready { cats = newCats, zoom = zoom } |> CatLoaded
                    


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case (msg, model) of
        (_, Error s) -> (Error s, Cmd.none)
        (Loaded (Err e), _) -> (Util.httpErrMsg e |> Error, Cmd.none)
        (Loaded (Ok stubs), _) ->
            let cats = List.map Cat.Prev stubs
            in (Ready { cats = cats, zoom = Nothing }, Cmd.none)
        (LoadCat stub, Loading) -> (Loading, Cmd.none)
        (LoadCat stub, Ready { cats, zoom }) ->
            let
                hydrator = hydrateCat cats zoom stub
                cmd =
                    Http.get
                        { url = stub.url
                        , expect = Http.expectString hydrator
                        }
            in (model, cmd)
        (CatLoaded Loading, _) -> (Error "this shouldn't happen", Cmd.none)
        (CatLoaded (Ready { cats, zoom }), _) -> (Ready { cats = cats, zoom = zoom }, Cmd.none)
        (CatLoaded (Error e), _) -> (Error e, Cmd.none)
        (Zoom sym, Ready { cats, zoom }) -> (Ready { cats = cats, zoom = Just sym}, Cmd.none)
        (Zoom _, Loading) -> (Error "this shouldn't happen", Cmd.none)
        (HideCat shown, Loading) -> (Error "this shouldn't happen", Cmd.none)
        (HideCat shown, Ready { cats, zoom }) ->
            (Ready { cats = Cat.hide shown cats, zoom = zoom }, Cmd.none)
        (ShowCat shown, Loading) -> (Error "this shouldn't happen", Cmd.none)
        (ShowCat shown, Ready { cats, zoom }) ->
            (Ready { cats = Cat.show shown cats, zoom = zoom}, Cmd.none)



-- View


view : Model -> Html Msg
view model =
    case model of
        Loading ->
            Html.div [] [ Html.text "Loading..." ]

        Error s ->
            Html.div [] [ Html.text s ]

        Ready { cats, zoom } ->
            let
                zoomDiv =
                    case zoom of
                        Nothing ->
                            Html.div [] []

                        Just sym ->
                            Sym.zoom sym
            in
            Html.div []
                [ Html.div [ Attributes.id "cats" ]
                    (List.map (Cat.view LoadCat HideCat ShowCat Zoom) cats)
                , zoomDiv
                ]
