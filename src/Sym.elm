module Sym exposing (Sym, fromLine, thumb, zoom)

import Hex
import Html exposing (Html)
import Html.Attributes as Attributes
import Html.Events as Events


type alias Sym =
    { value : Int
    , name : String
    }


symString : Sym -> String
symString s =
    Char.fromCode s.value |> String.fromChar

-- Show the "thumbnail" in the category view.
thumb : (Sym -> a) -> Sym -> Html a
thumb update s =
    Html.div
        [ Attributes.class "thumb"
        , Attributes.title s.name
        , Events.onClick (update s)
        ]
        [ Html.span [] [ symString s |> Html.text ] ]

-- Show the details when it's been clicked.'
zoom : Sym -> Html a
zoom s =
    Html.div
        [ Attributes.id "details" ]
        [ Html.span [ Attributes.class "big" ] [ symString s |> Html.text ]
        , Html.span [ Attributes.class "name" ] [ Html.text s.name ]
        , Html.span [ Attributes.class "sub" ]
            [ "0x" ++ Hex.toString s.value |> Html.text ]
        , Html.span [ Attributes.class "sub" ]
            [ String.fromInt s.value |> Html.text ]
        ]

-- Decode a `Sym` from a line of the category-specific data format.

fromLine : String -> Result String Sym
fromLine line =
    let
        chunks = String.split " " line
        value =
            List.head chunks
                |> Maybe.withDefault ""
                |> Hex.fromString
        name =
            List.tail chunks |> Maybe.map (String.join " ")
    in
    case (value, name) of
        (Ok n, Just aName) -> Ok (Sym n aName)
        (Err err, _ ) -> Err err
        (_, Nothing) -> Err "no name"
