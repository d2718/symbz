module Cat exposing (..)

import Html exposing (Html)
import Html.Attributes as Attributes
import Html.Events as Events
import Json.Decode as Decode exposing (Decoder)

import Sym exposing (Sym)
import Util

type alias Stub =
    { name: String
    , url: String
    }

type alias Shown =
    { name: String
    , url: String
    , symbols: List Sym
    }

type Cat
    = Prev Stub
    | Loaded Shown
    | Hidden Shown

decoder : Decoder Stub
decoder =
    Decode.map2 Stub
        (Decode.field "name" Decode.string)
        (Decode.field "url" Decode.string)

url : Cat -> String
url cat =
    case cat of
        Prev stub -> stub.url
        Loaded shown -> shown.url
        Hidden shown -> shown.url

hide : Shown -> List Cat -> List Cat
hide shown cats =
    Util.replace
        (\x y -> url x == shown.url)
        (Hidden shown)
        cats

show : Shown -> List Cat -> List Cat
show shown cats =
    Util.replace
        (\x y -> url x == shown.url)
        (Loaded shown)
        cats

-- Decoding the category data files

symFromLineErr : Int -> String -> String
symFromLineErr n err =
    String.join "" [ "line ", String.fromInt n, ": ", err ]

symFromLine : (Int, String) -> Result String Sym
symFromLine (n, line) =
    Sym.fromLine line |> Result.mapError (symFromLineErr n)

fromData : Stub -> String -> Result String Shown
fromData stub data =
    case String.split "\n" data
            |> List.filterMap
                (\line -> case String.trim line of
                            "" -> Nothing
                            _ -> Just line)
            |> Util.indexedTuples
            |> Util.rrrMap symFromLine of
        Ok syms -> Ok (Shown stub.name stub.url syms)
        Err err -> Err err

--

view : (Stub -> a) -> (Shown -> a) -> (Shown -> a) -> (Sym -> a) -> Cat -> Html a
view loadFn hideFn showFn zoomFn cat =
    case cat of
        Prev stub ->
            Html.div
                [ Attributes.class "unloaded-category"
                , Events.onClick (loadFn stub)
                ]
                [ Html.h2 []
                    [ Html.span [ Attributes.class "expand" ]
                        [ Html.text "[+] " ]
                    , Html.text stub.name 
                    ] 
                ]
        Loaded shown ->
            Html.div
                [ Attributes.class "category" ]
                [ Html.h2 [Events.onClick (hideFn shown)]
                    [ Html.text shown.name ]
                , Html.div [ Attributes.class "symbols" ]
                    (List.map (Sym.thumb zoomFn) shown.symbols)
                ]
        Hidden shown ->
            Html.div
                [ Attributes.class "hidden-category"
                , Events.onClick (showFn shown)
                ]
                [ Html.h2 []
                    [ Html.span [ Attributes.class "expand" ]
                        [ Html.text "[+] " ]
                    , Html.text shown.name
                    ]
                ]
